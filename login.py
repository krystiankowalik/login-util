from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec

from selenium.webdriver.support.wait import WebDriverWait
from time import sleep
import sys


def main():
    # Read user's input
    chrome_binary_location = sys.argv[1]
    chrome_driver_executable_path = sys.argv[2]
    initial_url = sys.argv[3]
    preferred_connection_location = sys.argv[4]

    chrome_options = webdriver.ChromeOptions()
    chrome_options.binary_location = chrome_binary_location

    driver = webdriver.Chrome(options=chrome_options,
                              executable_path=chrome_driver_executable_path)
    wait = WebDriverWait(driver, 20)

    # Force windows user agent
    driver.execute_cdp_cmd('Network.setUserAgentOverride', {
        "userAgent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36'})

    # Maximixe window to get a better look at the QR code
    driver.maximize_window()

    # Go to the landing page
    driver.get(initial_url)
    target_location_button_xpath = "/html/body/div[1]/nav/a[span='" + preferred_connection_location + "']"
    # Wait for the button with the appropriate connection location to be visible
    wait.until(ec.presence_of_element_located((By.XPATH, target_location_button_xpath)))
    # Find the button in the DOM
    target_location_button = driver.find_element_by_xpath(target_location_button_xpath)
    # Click the button / get redirected to the authentication method screen
    target_location_button.click()

    #
    login_button_xpath = "//a/span/strong[contains(text(),'ePass')]"
    # Wait for the button with the appropriate authentication method to be visible
    wait.until(ec.presence_of_element_located((By.XPATH, login_button_xpath)))
    # Find the button in the DOM
    login_button = driver.find_element_by_xpath(login_button_xpath)

    # Retry clicking the button until succeeded
    clicked = False
    while not clicked:
        try:
            login_button.click()
            print('Button clicked')
            clicked = True
        except WebDriverException:
            print('Button is not clickable yet, retrying')

    # Keep the page open for a long time - otherwise the connection will be broken
    sleep(60000000)


if __name__ == '__main__':
    main()
